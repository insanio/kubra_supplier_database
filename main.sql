drop schema if exists main cascade;

create schema if not exists main;

set search_path = main, pg_catalog;

drop table if exists level cascade;

create table level (
    id varchar(20) not null,
    parent varchar(20),
    application_level varchar(20),
    name varchar(100) not null,
    level int not null default 0,
    description text
);

alter table level add constraint pk_main_level primary key (id);
alter table level add constraint fk_main_level_application_level foreign key (application_level) references application.level (id);
alter table level add constraint fk_main_level_main_level foreign key (parent) references level (id);

drop table if exists "user" cascade;

create table "user" (
    id bigserial,
    username varchar(100),
    level varchar(20),
    name varchar(100) not null,
    description text
);

alter table "user" add constraint pk_main_user primary key (id);
-- alter table "user" add constraint uq_main_user unique (username);
alter table "user" add constraint fk_main_user_application_login foreign key (username) references application.login (username);
alter table "user" add constraint fk_main_user_main_level foreign key (level) references level (id);

drop table if exists user_property cascade;

create table user_property (
    id bigserial,
    parent bigint,
    code varhcar(20),
    "user" bigint not null,
    name varchar(50) not null,
    alias varchar(50) not null,
    value text,
    property_value int,
    description text
);

alter table user_property add constraint pk_main_user_property primary key (id);
alter table user_property add constraint fk_main_user_property_main_user foreign key ("user") references "user" (id);
alter table user_property add constraint fk_main_user_property_main_user_property foreign key (parent) references user_property (id);

drop table if exists user_contact cascade;

create table user_contact (
    "user" bigint not null,
    phone varchar(30) not null,
    mobile varchar(30) not null,
    email varchar(100) not null,
    location bigint not null,
    address text,
    description text
);

alter table user_contact add constraint pk_main_user_contact primary key ("user");
alter table user_contact add constraint fk_main_user_contact_main_user foreign key ("user") references "user" (id);
alter table user_contact add constraint fk_main_user_contact_master_location foreign key (location) references master.location (id);

-- End of basic (Main schema)

drop table if exists user_asset cascade;

create table user_asset (
    id bigserial,
    parent bigint,
    "user" bigint not null,
    name varchar(100) not null,
    note text,
    description text
);

alter table user_asset add constraint pk_main_user_asset primary key (id);
alter table user_asset add constraint fk_main_user_asset_main_user foreign key ("user") references "user" (id);
alter table user_asset add constraint fk_main_user_asset_main_user_asset foreign key (parent) references user_asset (id);

drop table if exists user_asset_type cascade;

create table user_asset_type (
    user_asset bigint not null,
    type int not null,
    level int not null default 0,
    description text
);

alter table user_asset_type add constraint pk_main_user_asset_type primary key (user_asset, type);
alter table user_asset_type add constraint fk_main_user_asset_type_main_user_asset foreign key (user_asset) references user_asset (id);
alter table user_asset_type add constraint fk_main_user_asset_type_master_asset_type foreign key (type) references master.asset_type (id);

drop table if exists user_asset_category cascade;

create table user_asset_category (
    user_asset bigint not null,
    category int not null,
    level int not null default 0,
    description text
);

alter table user_asset_category add constraint pk_main_user_asset_category primary key (user_asset, type);
alter table user_asset_category add constraint fk_main_user_asset_category_main_user_asset foreign key (user_asset) references user_asset (id);
alter table user_asset_category add constraint fk_main_user_asset_category_master_asset_category foreign key (category) references master.asset_category (id);

drop table if exists user_asset_contact cascade;

create table user_asset_contact (
    user_asset bigint not null,
    phone varchar(30) not null,
    mobile varchar(30) not null,
    email varchar(100) not null,
    location bigint not null,
    address text,
    note text,
    description text
);

alter table user_asset_contact add constraint pk_main_user_asset_contact primary key (user_asset);
alter table user_asset_contact add constraint fk_main_user_asset_contact_main_user_asset foreign key (user_asset) references user_asset (id);
alter table user_asset_contact add constraint fk_main_user_asset_contact_master_location foreign key (location) references master.location (id);

drop table if exists user_asset_property cascade;

create table user_asset_property (
    id bigserial,
    parent bigint,
    code varhcar(20),
    user_asset bigint not null,
    name varchar(50) not null,
    alias varchar(50) not null,
    value text,
    property_value int,
    description text
);

alter table user_asset_property add constraint pk_main_user_asset_property primary key (id);
alter table user_asset_property add constraint fk_main_user_asset_property_main_user_asset foreign key (user_asset) references user_asset (id);
alter table user_asset_property add constraint fk_main_user_asset_property_main_user_asset_property foreign key (parent) references user_asset_property (id);