drop schema if exists master cascade;

create schema if not exists master;

set search_path = master, pg_catalog;

drop table if exists location_type cascade;

create table location_type (
    id serial,
    name varchar(50) not null,
    description text
);

alter table location_type add constraint pk_master_location_type primary key (id);

drop table if exists location cascade;

create table location (
    id bigserial,
    parent bigint,
    name varchar(100) not null,
    type int not null,
    level int not null default 0,
    note text,
    description text
);

alter table location add constraint pk_master_location primary key (id);
alter table location add constraint fk_master_location_master_location foreign key (parent) references location (id);
alter table location add constraint fk_master_location_master_location_type foreign key (type) references location_type (id);

-- End of basic (Master schema)

drop table if exists asset_category cascade;

create table asset_category (
    id serial,
    code varchar(20),
    name varchar(50) not null,
    description text
);

alter table asset_category add constraint pk_master_asset_category primary key (id);

drop table if exists asset_type cascade;

create table asset_type (
    id serial,
    code varchar(20),
    name varchar(50) not null,
    description text
);

alter table asset_type add constraint pk_master_asset_type primary key (id);