insert into main."user" 
(name, description) 
values 
('Aris', 'Test data'), 
('Yuli', 'Test data'), 
('Adi', 'Test data'), 
('Novi', 'Test data'), 
('Ova', 'Test data'), 
('Radiansyah', 'Test data'), 
('Jack', 'Test data');

insert into master.asset_type 
(name, description, code) 
values 
('Toko', 'Test data', 'store');

insert into master.asset_category
(name, description) 
values
('Makanan', 'Test data'), 
('Minuman', 'Test data'), 
('Makanan dan Minuman', 'Test data'), 
('Jajanan', 'Test data'), 
('Fashion', 'Test data'), 
('Perlengkapan Pakaian', 'Test data');

insert into main.user_asset 
("user", parent, name, note, description) 
values 
(3, null, 'Sakinah', 'Toko bagus loooh', 'Test data'), 
(3, 1, 'Mawadah', 'Toko bagus loooh, Anak nya Toko Sakinah', 'Test data'), 
(3, 1, 'Wa Rahmah', 'Toko bagus loooh, Anak kedua nya Toko Sakinah', 'Test data'), 
(6, null, 'Best Friend', 'Toko di Marina', 'Test data'), 
(6, null, 'Food Good', 'Toko bagus loooh, Milik nya Radiansyah', 'Test data');

insert into main.user_asset_property 
(parent, user_asset, name, alias, value, description) 
values 
(null, 1, 'Map Latitude', 'map_latitude', '-7.317080', 'Test data'), 
(null, 1, 'Map Latitude', 'map_longitude', '112.749625', 'Test data'), 
(null, 2, 'Map Latitude', 'map_latitude', '-7.314407', 'Test data'), 
(null, 2, 'Map Latitude', 'map_longitude', '112.747229', 'Test data'), 
(null, 3, 'Map Latitude', 'map_latitude', '-7.317299', 'Test data'), 
(null, 3, 'Map Latitude', 'map_longitude', '112.747076', 'Test data'), 
(null, 4, 'Map Latitude', 'map_latitude', '-7.314807', 'Test data'), 
(null, 4, 'Map Latitude', 'map_longitude', '112.749656', 'Test data'), 
(null, 5, 'Map Latitude', 'map_latitude', '-7.314888', 'Test data'), 
(null, 5, 'Map Latitude', 'map_longitude', '112.747464', 'Test data');

insert into main.user_asset_property 
(parent, user_asset, name, alias, value, description) 
values 
(null, 1, 'Map Icon', 'map_icon', 'https://bamlive.s3.amazonaws.com/bampfa-store-interior.jpg', 'Test data'), 
(null, 2, 'Map Icon', 'map_icon', 'https://www.telegraph.co.uk/content/dam/news/2017/09/22/TELEMMGLPICT000141288947_trans_NvBQzQNjv4BqOMJrK9zxvZt2Dh-bEunLilGPptXXj_wfdgHWmO4ut70.jpeg?imwidth=480', 'Test data'), 
(null, 3, 'Map Icon', 'map_icon', 'https://www.supermarketnews.com/sites/supermarketnews.com/files/styles/article_featured_standard/public/Amazon_Go_first_Seattle_store.png?itok=mLLqz667', 'Test data'), 
(null, 4, 'Map Icon', 'map_icon', 'https://www.lego.com/r/www/r/stores/-/media/brand%20retail/stores/photos/new/600x450_0007_store%20front.jpg?l.r2=896430844', 'Test data'), 
(null, 5, 'Map Icon', 'map_icon', 'https://anewscafe.com/wp-content/uploads/2010/11/enjoy-the-store-1.jpeg', 'Test data');

insert into main.user_asset_property 
(parent, user_asset, name, alias, value, description) 
values 
(null, 1, 'Map Icon', 'map_image', 'https://bamlive.s3.amazonaws.com/bampfa-store-interior.jpg', 'Test data'), 
(null, 2, 'Map Icon', 'map_image', 'https://www.telegraph.co.uk/content/dam/news/2017/09/22/TELEMMGLPICT000141288947_trans_NvBQzQNjv4BqOMJrK9zxvZt2Dh-bEunLilGPptXXj_wfdgHWmO4ut70.jpeg?imwidth=480', 'Test data'), 
(null, 3, 'Map Icon', 'map_image', 'https://www.supermarketnews.com/sites/supermarketnews.com/files/styles/article_featured_standard/public/Amazon_Go_first_Seattle_store.png?itok=mLLqz667', 'Test data'), 
(null, 4, 'Map Icon', 'map_image', 'https://www.lego.com/r/www/r/stores/-/media/brand%20retail/stores/photos/new/600x450_0007_store%20front.jpg?l.r2=896430844', 'Test data'), 
(null, 5, 'Map Icon', 'map_image', 'https://anewscafe.com/wp-content/uploads/2010/11/enjoy-the-store-1.jpeg', 'Test data');

insert into application.level 
(id, parent, name, level, description) 
values 
('it-chief', null, 'Master of application', 0, 'Pre-insert');

insert into application.login_level 
(username, level, description) 
values 
('darkside', 'it-chief', 'Pre-insert');

insert into application.application 
(id, name, description) 
values 
('web-backoffice', 'Web admin Application', 'Pre-insert');

insert into application.privilege 
(id, name, general, description) 
values 
('insert', 'Insert data', true, 'Pre-insert'), 
('update', 'Update data', true, 'Pre-insert'), 
('delete', 'Delete data', true, 'Pre-insert'), 
('view', 'View data', true, 'Pre-insert'), 
('insert detail', 'Insert detail data', true, 'Pre-insert'), 
('update detail', 'Update detail data', true, 'Pre-insert'), 
('delete detail', 'Delete detail data', true, 'Pre-insert'), 
('view detail', 'View detail data', true, 'Pre-insert');

insert into main.level 
(id, name, description) 
values 
('user', 'User', 'Test data');

insert into internal.event_type
(name, description)
values
('Banner', 'Pre-Insert'),
('Berita', 'Pre-Insert'),
('Acara dan Kegiatan', 'Pre-Insert'),
('Promosi', 'Pre-Insert');

insert into internal.event
(type, name, title, event, banner, description)
values
(1, '1st Promotion test', 'Harga murah', 'Harga lagi murah loooh', 'https://www.bodleeds.co.uk/ekmps/shops/bodleeds/resources/Design/cheap-but-good.png', 'Pre-insert'),
(1, '2nd Promotion test', 'Harga terjangkau', 'G usah bingung, pasti nutut', 'https://cdn2.vectorstock.com/i/1000x1000/86/31/reasonable-price-sign-or-stamp-vector-18438631.jpg', 'Pre-insert'),
(1, '3rd Promotion test', 'Barang berkualitas', 'Barang nya bagus - bagus ~', 'https://innovatione.eu/wp-content/uploads/2014/09/approved-147677_640.png', 'Pre-insert');

insert into internal.event_detail
(event, detail_value, quota, description)
values
(1, extract(epoch from now()) * 1000, -1, 'Pre-insert'),
(2, extract(epoch from now()) * 1000, -1, 'Pre-insert'),
(3, extract(epoch from now()) * 1000, -1, 'Pre-insert');

insert into master.asset_type 
(name, description, code) 
values 
('Produk', 'Test data', 'product');

insert into main.user_asset 
("user", parent, name, note, description) 
values 
(3, 1, 'Baju', 'Baju besar dan lebar dan lapang', 'Test data'),
(3, 1, 'Celana', 'Celana besar dan lebar dan lapang', 'Test data');