drop schema if exists application cascade;

create schema if not exists application;

set search_path = application, pg_catalog;

drop table if exists level cascade;

create table level (
    id varchar(20) not null,
    parent varchar(20),
    name varchar(20) not null,
    level int not null default 0,
    description text
);

alter table level add constraint pk_application_level primary key (id);
alter table level add constraint fk_application_level_application_level foreign key (parent) references level (id);

drop table if exists login cascade;

create table login (
    username varchar(100) not null,
    password varchar(200) not null,
    description text
);

alter table login add constraint pk_application_login primary key (username);

drop table if exists login_detail cascade;

create table login_detail (
    username varchar(100) not null,
    description text,
    salt text,
    current_token text,
    current_token_expire integer,
    current_token_regenerate integer,
    current_token_regeneration_time bigint
);

alter table login_detail add constraint pk_application_login_detail primary key (username);
alter table login_detail add constraint fk_application_login_detail_application_login foreign key (username) references login (username) on update cascade on delete cascade;

drop table if exists login_property cascade;

create table login_property (
    id bigserial,
    parent bigint,
    username varchar(100) not null,
    code varchar(50) not null,
    name varchar(50) not null,
    value text,
    description text
);

alter table login_property add constraint pk_application_login_property primary key (id);
alter table login_property add constraint fk_application_login_property_application_login foreign key (username) references login (username);
alter table login_property add constraint fk_application_login_property_application_login_property foreign key (parent) references login_property (id);

drop table if exists login_level cascade;

create table login_level (
    username varchar(100) not null,
    level varchar(20) not null,
    description text
);

alter table login_level add constraint pk_application_login_level primary key (username, level);
alter table login_level add constraint fk_application_login_level_application_login foreign key (username) references login (username);
alter table login_level add constraint fk_application_login_level_application_level foreign key (level) references level (id);

drop table if exists application cascade;

create table application (
    id varchar(20) not null,
    name varchar(50) not null,
    description text
);

alter table application add constraint pk_application_application primary key (id);

drop table if exists module cascade;

create table module (
    id varchar(100) not null,
    name varchar(100) not null,
    description text
);

alter table module add constraint pk_application_module primary key (id);

drop table if exists privilege cascade;

create table privilege (
    id varchar(20) not null,
    name varchar(50) not null,
    general boolean not null default true,
    description text
);

alter table privilege add constraint pk_application_privilege primary key (id);

drop table if exists access cascade;

create table access (
    username varchar(100),
    level varchar(20) not null,
    privilege varchar(20) not null,
    module varchar(100) not null,
    application varchar(20) not null,
    access boolean not null default false,
    description text
);

alter table access add constraint uq_application_access unique (username, level, privilege, module, application);
alter table access add constraint fk_application_access_application_login_level foreign key (username, level) references login_level (username, level) on delete cascade;
alter table access add constraint fk_application_access_application_privilege foreign key (privilege) references privilege (id) on delete cascade;
alter table access add constraint fk_application_access_application_module foreign key (module) references module (id) on delete cascade on update cascade;
alter table access add constraint fk_application_access_application_application foreign key (application) references application (id) on delete cascade;

drop table if exists "table" cascade;

create table "table" (
    id varchar(50) not null,
    name varchar(50) not null,
    alias varchar(50) not null,
    description text
);

alter table "table" add constraint pk_application_table primary key (id);

drop table if exists table_property cascade;

create table table_property (
    id serial,
    parent int,
    "table" varchar(50) not null,
    code varchar(50) not null,
    name varchar(50) not null,
    value text,
    property_value int not null default 0,
    description text
);

alter table table_property add constraint pk_application_table_property primary key (id);
alter table table_property add constraint fk_application_table_property_application_table foreign key ("table") references "table" (id) on delete cascade;
alter table table_property add constraint fk_application_table_property_application_table_property foreign key (parent) references table_property (id) on delete cascade;

drop table if exists table_detail cascade;

create table table_detail (
    id serial,
    table_property int not null,
    code varchar(50) not null,
    name varchar(50) not null,
    value text,
    detail_value int not null default 0,
    description text
);

alter table table_detail add constraint pk_application_table_detail primary key (id);
alter table table_detail add constraint fk_application_table_detail_application_table_property foreign key (table_property) references table_property (id) on delete cascade;

drop table if exists menu cascade;

create table menu (
    id serial,
    parent int,
    module varchar(100),
    application varchar(20),
    "table" varchar(50),
    name varchar(50) not null,
    menu_description text,
    title varchar(50) not null,
    link text,
    url text,
    icon varchar(30),
    "group" boolean,
    expanded boolean,
    menu_value int not null default 0,
    description text
);

alter table menu add constraint pk_application_menu primary key (id);
alter table menu add constraint fk_application_menu_application_menu foreign key (parent) references menu (id) on delete cascade;
alter table menu add constraint fk_application_menu_application_module foreign key (module) references module (id) on delete cascade on update cascade;
alter table menu add constraint fk_application_menu_application_application foreign key (application) references application (id) on delete cascade;
alter table menu add constraint fk_application_menu_application_table foreign key ("table") references "table" (id) on delete cascade;

drop table if exists menu_property cascade;

create table menu_property (
    id serial,
    menu int not null,
    code varchar(50) not null,
    name varchar(50) not null,
    value text,
    description text
);

alter table menu_property add constraint pk_application_menu_property primary key (id);
alter table menu_property add constraint fk_application_menu_property_application_menu foreign key (menu) references menu (id) on delete cascade;

drop table if exists log cascade;

create table log (
    id bigserial,
    data_id text,
    process varchar(30) not null,
    "table" varchar(50),
    "schema" varchar(50),
    description text,
    created timestamp not null default now(),
    "user" varchar(100) not null
);

alter table log add constraint pk_application_log primary key (id);

drop table if exists log_detail cascade;

create table log_detail (
    id serial,
    log bigint,
    "column" varchar(50) not null,
    old_value text,
    new_value text
);

alter table log_detail add constraint pk_application_log_detail primary key (id);
alter table log_detail add constraint fk_application_log_detail_application_log foreign key (log) references log (id) on delete cascade;

drop table if exists record_addition cascade;

create table record_addition (
    id bigserial,
    "table" varchar(50),
    "schema" varchar(50),
    record text,
    name varchar(50) not null,
    value text,
    description text
);

alter table record_addition add constraint pk_main_record_addition primary key (id);
alter table record_addition add constraint uq_main_record_addition unique ("table", "schema", record, name);

drop table if exists setting cascade;

create table setting (
    id varchar(50) not null,
    parent varchar(50),
    name varchar(100) not null,
    value text,
    description text
);

alter table setting add constraint pk_main_setting primary key (id);