drop schema if exists internal cascade;

create schema if not exists internal;

set search_path = internal, pg_catalog;

drop table if exists event_type cascade;

create table event_type (
    id serial,
    name varchar(50) not null,
    description text
);

alter table event_type add constraint pk_internal_event_type primary key (id);

drop table if exists event cascade;

create table event (
    id serial,
    parent int,
    type int not null,
    name varchar(50) not null,
    title varchar(100) not null,
    event text,
    banner text,
    description text
);

alter table event add constraint pk_internal_event primary key (id);
alter table event add constraint fk_internal_event_internal_event foreign key (parent) references event (id);
alter table event add constraint fk_internal_event_internal_event_type foreign key (type) references event_type (id);

drop table if exists event_detail cascade;

create table event_detail (
    event int not null,
    detail_value bigint,
    description text,
    active boolean not null default true,
    coordinator bigint,
    banner_time_start timestamp,
    banner_time_end timestamp,
    registration_time_start timestamp,
    registration_time_end timestamp,
    time_start timestamp,
    time_end timestamp,
    quota int not null default 0
);

alter table event_detail add constraint pk_internal_event_detail primary key (event);
alter table event_detail add constraint fk_internal_event_detail_internal_event foreign key (event) references event (id);
alter table event_detail add constraint fk_internal_event_detail_main_user foreign key (coordinator) references main."user" (id);

drop table if exists event_property cascade;

create table event_property (
    id serial,
    parent int,
    event int not null,
    name varchar(50) not null,
    alias varchar(50) not null,
    property_value int,
    value text,
    description text
);

alter table event_property add constraint pk_internal_event_property primary key (id);
alter table event_property add constraint fk_internal_event_property_internal_event foreign key (event) references event (id);
alter table event_property add constraint fk_internal_event_property_internal_event_property foreign key (parent) references event_property (id);

drop table if exists event_user cascade;

create table event_user (
    event int not null,
    "user" bigint not null,
    description text,
    user_parent bigint not null,
    registered_time timestamp not null default now(),
    expiration_time timestamp
);

alter table event_user add constraint pk_internal_event_user primary key (event, "user");
alter table event_user add constraint fk_internal_event_user_internal_event foreign key (event) references event (id);
alter table event_user add constraint fk_internal_event_user_main_user foreign key ("user") references main."user" (id);
alter table event_user add constraint fk_internal_event_user_main_user_1 foreign key (user_parent) references main."user" (id);