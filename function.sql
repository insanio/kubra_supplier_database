drop schema if exists "function" cascade;

create schema if not exists "function";

set search_path = "function", pg_catalog;

drop function if exists trigfunc_aftins_login_level() cascade;

create or replace function trigfunc_aftins_login_level() returns trigger
    language plpgsql
    as $$
begin
insert into application.access (username, level, privilege, module, application, description) select new.username, new.level, p.id, m.id, a.id, 'Auto insert by "trigfunc_aftins_login_level" Trigger' from application.privilege as p, application.module as m, application.application as a;
return null;
end;
$$;

drop trigger if exists trig_aftins_login_level on application.login_level cascade;

create trigger trig_aftins_login_level after insert on application.login_level for each row execute procedure trigfunc_aftins_login_level();

drop function if exists trigfunc_aftins_level() cascade;

create or replace function trigfunc_aftins_level() returns trigger
    language plpgsql
    as $$
begin
insert into application.access (level, privilege, module, application, description) select new.id, p.id, m.id, a.id, 'Auto insert by "trigfunc_aftins_level" Trigger' from application.privilege as p, application.module as m, application.application as a;
return null;
end;
$$;

drop trigger if exists trig_aftins_level on application.level cascade;

create trigger trig_aftins_level after insert on application.level for each row execute procedure trigfunc_aftins_level();

drop function if exists trigfunc_aftins_application() cascade;

create or replace function trigfunc_aftins_application() returns trigger
    language plpgsql
    as $$
begin
insert into application.access (level, privilege, module, application, description) select l.id, p.id, m.id, new.id, 'Auto insert by "trigfunc_aftins_application" Trigger' from application.privilege as p, application.module as m, application.level as l;
insert into application.access (username, level, privilege, module, application, description) select la.username, la.level, p.id, m.id, new.id, 'Auto insert by "trigfunc_aftins_application" Trigger' from application.privilege as p, application.module as m, application.login_level as la;
return null;
end;
$$;

drop trigger if exists trig_aftins_application on application.application cascade;

create trigger trig_aftins_application after insert on application.application for each row execute procedure trigfunc_aftins_application();

drop function if exists trigfunc_aftins_module() cascade;

create or replace function trigfunc_aftins_module() returns trigger
    language plpgsql
    as $$
begin
insert into application.access (level, privilege, module, application, description) select l.id, p.id, new.id, a.id, 'Auto insert by "trigfunc_aftins_module" Trigger' from application.privilege as p, application.application as a, application.level as l;
insert into application.access (username, level, privilege, module, application, description) select la.username, la.level, p.id, new.id, a.id, 'Auto insert by "trigfunc_aftins_module" Trigger' from application.privilege as p, application.application as a, application.login_level as la;
return null;
end;
$$;

drop trigger if exists trig_aftins_module on application.module cascade;

create trigger trig_aftins_module after insert on application.module for each row execute procedure trigfunc_aftins_module();

drop function if exists trigfunc_aftins_privilege() cascade;

create or replace function trigfunc_aftins_privilege() returns trigger
    language plpgsql
    as $$
begin

if new.general then
insert into application.access (level, privilege, module, application, description) select l.id, new.id, m.id, a.id, 'Auto insert by "trigfunc_aftins_privilege" Trigger' from application.module as m, application.application as a, application.level as l;
insert into application.access (username, level, privilege, module, application, description) select la.username, la.level, new.id, m.id, a.id, 'Auto insert by "trigfunc_aftins_privilege" Trigger' from application.module as m, application.application as a, application.login_level as la;
end if;

return null;
end;
$$;

drop trigger if exists trig_aftins_privilege on application.privilege cascade;

create trigger trig_aftins_privilege after insert on application.privilege for each row execute procedure trigfunc_aftins_privilege();

drop function if exists trigfunc_aftupd_access() cascade;

create or replace function trigfunc_aftupd_access() returns trigger
    language plpgsql
    as $$
declare
acc boolean;
begin

if old.username is null and new.username is null then
update application.access set access = new.access where level = new.level and username is not null;
elseif old.username is not null and new.username is not null then
select access into acc from application.access where level = new.level and username is null;
if new.access and acc = false then
raise exception 'childexceedparenterror, Child (Login) access cannot exceed Parent (Level) access!';
end if;
end if;

return new;
end;
$$;

drop trigger if exists trig_aftupd_access on application.access cascade;

create trigger trig_aftupd_access after update on application.access for each row execute procedure trigfunc_aftupd_access();